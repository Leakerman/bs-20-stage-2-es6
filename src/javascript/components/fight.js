import { controls } from "../../constants/controls";

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const pressedKeys = new Set();

    firstFighter.state = {
      blockKey: controls.PlayerOneBlock,
      canCrit: true,
      position: "left",
    };

    secondFighter.state = {
      blockKey: controls.PlayerTwoBlock,
      canCrit: true,
      position: "right",
    };

    runFunctionOnKeys(
      pressedKeys,
      () =>
        fighterAttack(firstFighter, secondFighter, resolve, false, pressedKeys),
      [controls.PlayerOneAttack]
    );

    runFunctionOnKeys(
      pressedKeys,
      () =>
        fighterAttack(secondFighter, firstFighter, resolve, false, pressedKeys),
      [controls.PlayerTwoAttack]
    );

    runFunctionOnKeys(
      pressedKeys,
      () =>
        fighterAttack(firstFighter, secondFighter, resolve, true, pressedKeys),
      controls.PlayerOneCriticalHitCombination
    );

    runFunctionOnKeys(
      pressedKeys,
      () =>
        fighterAttack(secondFighter, firstFighter, resolve, true, pressedKeys),
      controls.PlayerTwoCriticalHitCombination
    );
  });
}

const keyUpHandler = (event, pressedKeys) => {
  if (event.repeat) return;
  console.log("up", event);

  pressedKeys.delete(event.code);
};

const keyDownHandler = (event, func, codes, pressedKeys) => {
  if (event.repeat) return;
  console.log("down", event);

  pressedKeys.add(event.code);
  // is all keys pressed?
  for (let code of codes) {
    if (!pressedKeys.has(code)) return;
  }
  func();
};

function runFunctionOnKeys(pressedKeys, func, codes) {
  console.log("sss", this);

  document.addEventListener("keydown", (e) =>
    keyDownHandler(e, func, codes, pressedKeys)
  );
  document.addEventListener("keyup", (e) => keyUpHandler(e, pressedKeys));
}

function fighterAttack(attacker, defender, resolve, isCrit, pressedKeys) {
  // atacker can not attack from block
  if (pressedKeys.has(defender.state.blockKey) && !isCrit) {
    console.log(`${defender.name} blocked`);
    return;
  }

  const canCrit = attacker.state.canCrit;
  const isAttackerBlocks = pressedKeys.has(attacker.state.blockKey);

  let damage = 0;
  if (isCrit && canCrit && !isAttackerBlocks) {
    setCritDelay(attacker);

    damage = getCriticalHitPower(attacker);
  }
  if (!isCrit && !isAttackerBlocks) {
    damage = getDamage(attacker, defender);
  }

  if (!damage) {
    console.log("Dodged");
  } else {
    console.log(`${defender.name} got ${damage} damage`);
  }

  const isDefeated = decreaseHealthBar(
    defender,
    damage,
    defender.state.position
  );

  if (isDefeated) {
    resolve(attacker);
  }
}

function setCritDelay(fighter) {
  fighter.state.canCrit = false;

  setTimeout(() => {
    console.log(`${fighter.state.position} fighter can crit`);
    fighter.state.canCrit = true;
  }, 10000);
}

function decreaseHealthBar(fighter, damage, position) {
  const healthBar = document.getElementById(`${position}-fighter-indicator`);
  const healthBarWidth = healthBar.style.width || 100;
  const decreasedHealthBarWidth =
    parseFloat(healthBarWidth) - (damage / fighter.health) * 100;

  healthBar.style.width =
    decreasedHealthBarWidth > 0 ? `${decreasedHealthBarWidth}%` : 0;
  // check is fighter defeated
  return decreasedHealthBarWidth < 0;
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter) {
  const randomNumber = Math.random() + 1;
  const { attack } = fighter;
  return randomNumber * attack;
}

export function getCriticalHitPower(fighter) {
  return fighter.attack * 2;
}

export function getBlockPower(fighter) {
  const randomNumber = Math.random() + 1;
  const { defense } = fighter;
  return randomNumber * defense;
}
