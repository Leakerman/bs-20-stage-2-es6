import { showModal } from "./modal";
import { createFighterImage } from "../fighterPreview";

export function showWinnerModal(fighter) {
  const title = `${fighter.name} won!`;
  const bodyElement = createFighterImage(fighter);
  const onClose = () => document.location.reload(true);
  showModal({ title, bodyElement, onClose });
}
