import { createElement } from "../helpers/domHelper";

export function createFighterPreview(fighter, position) {
  const positionClassName =
    position === "right" ? "fighter-preview___right" : "fighter-preview___left";
  const fighterElement = createElement({
    tagName: "div",
    className: `fighter-preview___root card ${positionClassName}`,
  });

  if (!fighter) return fighterElement;

  const image = createFighterImage(fighter);
  const content = createFighterContent(fighter);

  fighterElement.append(image, content);

  return fighterElement;
}

function createFighterContent(fighter) {
  const { health, attack, defense } = fighter;
  
  const content = createElement({
    tagName: "ul",
    className: "fighter-preview___content",
  });

  const healthItem = createFighterContentItem("Health", health);
  const attackItem = createFighterContentItem("Attack", attack);
  const defenseItem = createFighterContentItem("Defence", defense);

  content.append(healthItem, attackItem, defenseItem);

  return content;
}

function createFighterContentItem(name, value) {
  const item = createElement({ tagName: "li" });
  item.innerHTML = `<b>${name}</b>: ${value}`
  return item;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: "img",
    className: "fighter-preview___img",
    attributes,
  });

  return imgElement;
}
